package com.ugb.msproducts.dao;

import com.ugb.msproducts.models.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<ProductModel, Long> {
}
