package com.ugb.msproducts.controller;

import com.ugb.msproducts.dao.ProductDao;
import com.ugb.msproducts.dto.ProductRequestDto;
import com.ugb.msproducts.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ProductController {

  @Autowired
  private ProductDao productDao;

  @GetMapping(path = "/api/products/list")
  public ResponseEntity<List<ProductModel>> list() {
    return ResponseEntity.status(HttpStatus.OK).body(productDao.findAll());
  }

  @PostMapping(path = "/api/products/create")
  public ResponseEntity<ProductModel> create(@RequestBody ProductRequestDto productRequest) {
    ProductModel newProduct = new ProductModel();

    newProduct.setName(productRequest.getName());
    newProduct.setDescription(productRequest.getDescription());
    newProduct.setPrice(productRequest.getPrice());
    newProduct.setQuantity(productRequest.getQuantity());

    productDao.save(newProduct);

    return ResponseEntity.status(HttpStatus.CREATED).body(newProduct);
  }
}
